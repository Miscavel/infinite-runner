import "phaser";

export default {
    cloud: {
        initialVelocity: new Phaser.Math.Vector2(-12.5, 0), //Speed at which the cloud is moving
    },
    mountain: {
        initialVelocity: new Phaser.Math.Vector2(-37.5, 0), //Speed at which the mountain is moving 
    },
    imgCount: 3 //Number of images to be instantiated. In this case, 3 images are instantiated and recycled everytime one goes off screen
}