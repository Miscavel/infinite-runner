export default {
    jump: {
        name: 'jump',
        url: 'src/Assets/Sound/jump.wav'
    },
    coin: {
        name: 'coin',
        url: 'src/Assets/Sound/pickup.mp3'
    },
    obstacle: {
        name: 'obstacle',
        url: 'src/Assets/Sound/bounce.mp3'
    }
}