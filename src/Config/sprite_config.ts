//Configuration for each spritesheet
export const PlayerSpriteConfig = {
    name: 'player',
    url: 'src/Assets/Tilesheet/platformerPack_character.png',
    frame: {
        frameWidth: 96,
        frameHeight: 96
    }
};

export const TileSpriteConfig = {
    name: 'tile',
    url: 'src/Assets/Tilesheet/platformPack_tilesheet.png',
    frame: {
        frameWidth: 64,
        frameHeight: 64
    },
    tile: {
        ground: 0,
        bedrock: 3,
        blank: 12,
        sun: 25,
        coin: 38,
        fence: 57
    },
    extraProp: {
        sun: {
            spinDelay: 20,
            spinRotation: -7
        }
    } 
};

export const ParallaxSpriteConfig = {
    cloud: {
        name: 'cloud',
        url: 'src/Assets/PNG/Parallax/cloud.jpg',
        dimension: {x: 1000, y: 1080}
    },
    mountain: {
        name: 'mountain',
        url: 'src/Assets/PNG/Parallax/mountain_1.png',
        dimension: {x: 2048, y: 770}
    }
};