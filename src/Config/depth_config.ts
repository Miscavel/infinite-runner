//Depth sorting index, smaller index is rendered first (behind)
export default {
    cloud: -2,
    mountain: -1,
    brick: 0,
    player: 1,
    interactive: 2
}