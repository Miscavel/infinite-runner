import "phaser";

export const DEFAULT_WIDTH: number = 1200;
export const DEFAULT_HEIGHT: number = 800;

const config: Phaser.Types.Core.GameConfig = {
  title: "Infinite Runner",
  scale: {
    parent: "game",
    mode: Phaser.Scale.FIT,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: DEFAULT_WIDTH,
    height: DEFAULT_HEIGHT
  },
  physics: {
    default: "arcade",
    arcade: {
      debug: false
    }
  },
  backgroundColor: "#8BD6E0"
};

export default config;