import "phaser";

export default {
    jumpVelocity: new Phaser.Math.Vector2(0, -600 * 0.9), //Dictates how high the player jumps (more negative == higher)
    gravity: new Phaser.Math.Vector2(0, 980), //Dictates how fast the player falls (more positive == faster)
    status: {
        running: 'running',
        jumping: 'jumping',
        crouching: 'crouching'
    },
    duration: {
        crouching: 1000 //Crouching duration
    },
    hitbox: {
        normal: new Phaser.Math.Vector2(0.8, 0.8),
        crouching: new Phaser.Math.Vector2(0.8, 0.4)
    }
}