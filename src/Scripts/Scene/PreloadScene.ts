import "phaser";
import { PreloadSceneKey, GameSceneKey } from '../../Config/scene_config';
import { PlayerSpriteConfig, TileSpriteConfig, ParallaxSpriteConfig } from '../../Config/sprite_config';
import { PlayerAnimationConfig } from '../../Config/animation_config';
import AudioConfig from '../../Config/audio_config';

export class PreloadScene extends Phaser.Scene {
    constructor() {
        super({
            key: PreloadSceneKey
        });
    }

    loadSpritesheets(): void {
        //Load player spritesheet
        this.load.spritesheet(PlayerSpriteConfig.name, PlayerSpriteConfig.url, PlayerSpriteConfig.frame);
        
        //Load tile spritesheet
        this.load.spritesheet(TileSpriteConfig.name, TileSpriteConfig.url, TileSpriteConfig.frame);

        //Load parallax images
        this.load.image(ParallaxSpriteConfig.cloud.name, ParallaxSpriteConfig.cloud.url);
        this.load.image(ParallaxSpriteConfig.mountain.name, ParallaxSpriteConfig.mountain.url);
    }

    generateAnimations(): void {
        //Generate player animations
        PlayerAnimationConfig.forEach(function (value) {
            this.anims.create({
                key: value.key,
                frames: this.anims.generateFrameNumbers(PlayerSpriteConfig.name, {frames: value.frames}),
                frameRate: value.frameRate,
                repeat: value.repeat
            });
        }.bind(this));
    }
    
    loadAudio(): void {
        //Load sound effects used in game
        this.load.audio(AudioConfig.jump.name, AudioConfig.jump.url);
        this.load.audio(AudioConfig.coin.name, AudioConfig.coin.url);
        this.load.audio(AudioConfig.obstacle.name, AudioConfig.obstacle.url);   
    }

    preload(): void {
        this.loadSpritesheets();
        this.loadAudio();

        //Once all assets are loaded, go to game scene
        this.load.on('complete', function () {
            this.generateAnimations();
            this.scene.start(GameSceneKey);
        }.bind(this));
    }

    create(): void {
        
    }
}