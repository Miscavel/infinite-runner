import "phaser";
import { TileSpriteConfig } from '../../Config/sprite_config';
import PlatformConfig from '../../Config/platform_config';
import Brick from './Brick';
import InteractiveConfig, { InteractiveType } from '../../Config/interactive_config';
import Interactive from './Interactive';

export default class Platform {
    /*
        Platform class is used to bind the interacting objects (Brick, Coin, Obstacle) together.
        
        ### Brick-related stuff ###
        'platform' group: used to contain bedrock layers for visuals.
        'platformCollider' group: used to contain ground layers that keeps the player afoot.
        'lastBrick': used to remember which brick is at the furthest position. Useful when we want
        to reposition bricks that are out of view.
        'brickArr': array of brick objects. Used mainly to check which bricks are out of view and
        need to be recycled.

        ## Interactive-related stuff ###
        'interactiveGroup': used to contain interactive objects so that we can create callbacks
        on collision with player
        'interactiveArr': pooling for interactive objects, contain interactive objects that 
        are ready to be recycled (not active)
        'tick': a counter to decide whether to spawn an interactive object or not
    */
    platform: Phaser.Physics.Arcade.Group;
    platformCollider: Phaser.Physics.Arcade.Group;
    lastBrick: Brick;
    brickArr: Array<Brick>;

    interactiveGroup: Phaser.Physics.Arcade.Group;
    interactiveArr: Array<Interactive>;
    tick: number;

    constructor(scene: Phaser.Scene) {
        //Group setup for brick objects
        this.platform = scene.physics.add.group();
        this.platformCollider = scene.physics.add.group();
        this.brickArr = [];
        
        //Interactive group for coin and obstacles
        this.interactiveGroup = scene.physics.add.group();
        this.interactiveArr = [];
        this.tick = 0;
    }

    update(): void {
        this.recycleBricks();
    }

    generateBricks(dimension: {width: number, height: number}): void {
        /*
            Function to generate the initial bricks.
            The number of inital bricks can be changed from PlatformConfig.
        */
        for (let index = 0; index < PlatformConfig.initialCount; index++) {
            const x = TileSpriteConfig.frame.frameWidth * (0.5 + index);
            const y = dimension.height - TileSpriteConfig.frame.frameHeight * 2.5;
            
            const brick: Brick = new Brick(x, y, this);
            brick.setVelocity(PlatformConfig.initialVelocity);
            this.brickArr.push(brick);
        }
        this.lastBrick = this.brickArr[this.brickArr.length - 1];
    }

    recycleBricks(): void {
        //Function to reposition bricks that are out of view
        this.brickArr.forEach(function (brick: Brick) {
            //A brick which 'x' value is below that mentioned in config is repositioned
            if (brick.top.x < PlatformConfig.leftBoundary.x) {
                //Reposition brick next to the furthest brick
                const x = this.lastBrick.top.x + TileSpriteConfig.frame.frameWidth;
                const y = this.lastBrick.top.y;
                brick.reposition(x, y, this.interactiveArr);

                //Update the furthest brick
                this.lastBrick = brick;

                if (this.tick < InteractiveConfig.step)
                {
                    /*
                        Updates tick until it reaches the number set in config.
                        e.g. if config sets that an obstacle appears on every 10th brick, then 
                        increment tick until it hits 10
                    */
                    this.tick++;
                }
                else
                {
                    //Generate random interactive object
                    let object = InteractiveConfig.objects[Phaser.Math.RND.between(0, InteractiveConfig.objects.length - 1)];
                    let interactive: Interactive;

                    if (this.interactiveArr.length > 0)
                    {
                        //If there is an object in the pool, reuse it and reposition
                        interactive = this.interactiveArr.pop();
                        interactive.relocate(x, y, this, object, brick.top.body.velocity);
                    }
                    else
                    {
                        //Otherwise, create a new interactive object
                        interactive = new Interactive(x, y, this, object, brick.top.body.velocity);
                    }
                    brick.setInteractive(interactive);
                    this.tick = 0;
                }
            }
        }.bind(this));
    }

    //Stop objects from moving, used on gameOver
    stop(): void {
        this.stopGroup(this.platform);
        this.stopGroup(this.platformCollider);
        this.stopGroup(this.interactiveGroup);
    }

    stopGroup(group: Phaser.Physics.Arcade.Group): void {
        group.children.each(function (item: Phaser.Physics.Arcade.Image) {
            item.setVelocity(0, 0);
        });
    }
}