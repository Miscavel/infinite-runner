import "phaser";
import { TileSpriteConfig } from '../../Config/sprite_config';
import Platform from './Platform';
import Brick from "./Brick";
import DepthConfig from '../../Config/depth_config';

export default class Interactive {
    /*
        Interactive class for obstacles and coin.
        When instantiated, an interactive object is always tied to a brick.
        This is done to allow easier positioning and recycling into inactive pool in Platform.

        'timedEvent' is used exclusively for sun obstacle which has a spinning animation.
        'brick' refers to the parent brick of the given interactive object.
    */
    sprite: Phaser.Physics.Arcade.Image;
    timedEvent: Phaser.Time.TimerEvent;
    brick: Brick;

    constructor(x: number, y: number, platform: Platform, property: {type: string, offset: {x: number, y: number}, index: number}, velocity: {x: number, y: number}) {
        this.generateInteractive(x, y, platform, property, velocity);
    }

    generateInteractive(x: number, y: number, platform: Platform, property: {type: string, offset: {x: number, y: number}, index: number}, velocity: {x: number, y: number}): void {
        this.sprite = platform.interactiveGroup.create(x + property.offset.x, y + property.offset.y, TileSpriteConfig.name, property.index);
        this.sprite.setDepth(DepthConfig.interactive);
        this.setup(velocity, property.index, property.type, platform);
    }

    relocate(x: number, y: number, platform: Platform, property: {type: string, offset: {x: number, y: number}, index: number}, velocity: {x: number, y: number}) {
        this.sprite.setTexture(TileSpriteConfig.name, property.index);
        this.sprite.setPosition(x + property.offset.x, y + property.offset.y);
        this.setup(velocity, property.index, property.type, platform);
    }

    //Pre-setup after object is created / relocated
    setup(velocity: {x: number, y: number}, index: number, type: string, platform: Platform): void {
        this.sprite.setAngle(0); //Resets angle back to 0, in case it got rotated (sun)
        this.sprite.setData('type', type); //Sets the interactive type to either 'obstacle' or 'coin'
        this.sprite.setVelocity(velocity.x, velocity.y); 
        this.sprite.body.checkCollision.none = false; //Enables collision back in case it was turned off (coin)
        this.sprite.setActive(true);
        this.sprite.setVisible(true); //Turns on visibility in case it was turned off (coin)
        if (index == TileSpriteConfig.tile.sun)
        {
            //Rotate the sun at a specified delay on a loop
            this.timedEvent = platform.platform.scene.time.addEvent({
                delay: TileSpriteConfig.extraProp.sun.spinDelay,
                callback: function () {
                    //Angle after rotation, %360 to prevent angle from getting too large (overflow)
                    const resultantAngle = (this.sprite.angle + TileSpriteConfig.extraProp.sun.spinRotation) % 360;
                    this.sprite.setAngle(resultantAngle);
                }.bind(this),
                loop: true
            })
        }
    }

    disable(): void {
        this.sprite.setActive(false);
        if (this.timedEvent != null)
        {
            //Remove timedEvent if there is any (e.g. sun rotation)
            this.timedEvent.remove();
        }
    }
}