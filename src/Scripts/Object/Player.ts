import "phaser";
import { PlayerSpriteConfig } from '../../Config/sprite_config';
import { PlayerAnimationList } from '../../Config/animation_config';
import PlayerConfig from '../../Config/player_config';
import DepthConfig from '../../Config/depth_config';
import AudioConfig from '../../Config/audio_config';

export default class Player {
    /*
        Player class for the character that the player controls.
        'sprite': the character.
        'status': used for conditionals to check if a player is able to jump or crouch.
        The list can be found in config, with current settings being: [running, jumping, crouching]
    */
    sprite: Phaser.Physics.Arcade.Sprite;
    status: string = PlayerConfig.status.jumping;

    constructor(position: {x: number, y: number}, physics: Phaser.Physics.Arcade.ArcadePhysics) {
        this.sprite = physics.add.sprite(position.x, position.y, PlayerSpriteConfig.name, 0);
        this.sprite.setDepth(DepthConfig.player);
        this.adjustHitbox(PlayerConfig.hitbox.normal);
        this.setGravity(PlayerConfig.gravity);
        this.run();
    }

    adjustHitbox(ratio: Phaser.Math.Vector2): void {
        /*
            Function to adjust player hitbox size.
            Used when reducing player's hitbox height when crouching, and restoring the hitbox back
            upon standing up.
        */
        this.sprite.setSize(PlayerSpriteConfig.frame.frameWidth * ratio.x, PlayerSpriteConfig.frame.frameHeight * ratio.y);
        this.sprite.setOffset(PlayerSpriteConfig.frame.frameWidth * ((1 - ratio.x) / 2), PlayerSpriteConfig.frame.frameHeight * (1 - ratio.y));
    }

    run(): void {
        // Sets the character into a running state.
        this.sprite.play(PlayerAnimationList.running, true);
        this.setStatus(PlayerConfig.status.running);
    }

    jump(scene: Phaser.Scene): void {
        /*
            Makes the character jump.
            Jump is only allowed when the player is in running state.
        */
        if (this.status === PlayerConfig.status.running)
        {
            this.sprite.setVelocity(this.sprite.body.velocity.x, PlayerConfig.jumpVelocity.y);
            this.sprite.play(PlayerAnimationList.jumping);
            this.setStatus(PlayerConfig.status.jumping);

            //Plays the jump sound only if player jumps through input (exclude death jump)
            if (scene)
                scene.sound.play(AudioConfig.jump.name);
        }
    }

    crouch(scene: Phaser.Scene): void {
        /*
            Makes the character crouch.
            Jump is only allowed when the player is in running state.
        */
        if (this.status === PlayerConfig.status.running)
        {
            this.sprite.play(PlayerAnimationList.crouching);
            this.setStatus(PlayerConfig.status.crouching);

            //Reduces player's hitbox height
            this.adjustHitbox(PlayerConfig.hitbox.crouching);
            scene.time.addEvent({
                delay: PlayerConfig.duration.crouching,
                callback: function () {
                    //Switch back to running state
                    this.run();

                    //Restores player's hitbox height
                    this.adjustHitbox(PlayerConfig.hitbox.normal);
                }.bind(this)
            });
        }
    }

    touchGround(): void {
        /*
            Callback when the player touches ground.
            Sets the player state from jumping back to running.
        */
        if (this.status === PlayerConfig.status.jumping)
        {
            this.run();
        }
    }

    setStatus(state: string) {
        this.status = state;
    }

    //Sets player's gravity
    setGravity(vector: Phaser.Math.Vector2): void {
        this.sprite.setGravity(vector.x, vector.y);
    }

    //Makes player jump upon death
    kill(): void {
        this.jump(null);
    }
}