import "phaser";
import config from './Config/config';
import { PreloadScene } from './Scripts/Scene/PreloadScene';
import { GameScene } from './Scripts/Scene/GameScene';
import { PreloadSceneKey, GameSceneKey } from './Config/scene_config';

export class InfiniteRunner extends Phaser.Game {
    constructor(config: Phaser.Types.Core.GameConfig) {
        super(config);
        this.scene.add(PreloadSceneKey, PreloadScene);
        this.scene.add(GameSceneKey, GameScene);
        this.scene.start(PreloadSceneKey);
    }
}

window.onload = () => {
    var game = new InfiniteRunner(config);
};